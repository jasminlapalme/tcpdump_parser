require 'spec_helper'

describe TcpdumpParser do
  it 'has a version number' do
    expect(TcpdumpParser::VERSION).not_to be nil
  end

  it 'returns nil on empty string' do
    expect(TcpdumpParser::parse_line("")).to eq(nil)
  end

  it 'parses line from tcpdump' do
    expect(TcpdumpParser::parse_line("2016-05-14 15:40:01.703261 54:e4:3a:e7:39:3c > 20:c9:d0:48:f6:0d, ethertype IPv4 (0x0800), length 414: 207.115.108.42.4500 > 10.0.1.13.4500: UDP-encap: ESP(spi=0x0d9498de,seq=0x28003), length 372")).to eq({
      date_time: DateTime.rfc3339("2016-05-14T15:40:01-04:00").to_time,
      mac_addr_to: "20:C9:D0:48:F6:0D",
      ip_addr_to: "10.0.1.13",
      mac_addr_from: "54:E4:3A:E7:39:3C",
      ip_addr_from: "207.115.108.42",
      length: 414})
  end
  
  it 'calls tcpdump and stops after false' do
    calls_count = 0
    TcpdumpParser::listen_to('en0') do |info|
      calls_count = calls_count + 1
      false
    end
    expect(calls_count).to eq(1)
  end
end
